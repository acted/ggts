#!/usr/bin/env python3

import argparse
import os
import queue
import sounddevice as sd
import vosk
import sys
import Levenshtein
import json
import pyttsx3
import time

q = queue.Queue()

def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text

def callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)
    q.put(bytes(indata))

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument(
    '-l', '--list-devices', action='store_true',
    help='show list of audio devices and exit')
args, remaining = parser.parse_known_args()
if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[parser])
parser.add_argument(
    '-m', '--model', type=str, metavar='MODEL_PATH',
    help='Path to the model')
parser.add_argument(
    '-d', '--device', type=int_or_str,
    help='input device (numeric ID or substring)')
parser.add_argument(
    '-r', '--samplerate', type=int, help='sampling rate')
parser.add_argument(
    '-w', '--word', type=str, help='word to say')
args = parser.parse_args(remaining)

try:
    if args.model is None:
        args.model = "vosk/model/vosk-model-small-cn-0.3"
    if not os.path.exists(args.model):
        print ("Please download a model for your language from https://alphacephei.com/vosk/models")
        print ("and unpack as 'model' in the current folder.")
        parser.exit(0)
    if args.samplerate is None:
        device_info = sd.query_devices(args.device, 'input')
        # soundfile expects an int, sounddevice provides a float:
        args.samplerate = int(device_info['default_samplerate'])
    if args.word is None:
        args.word='我是绒布球'

    model = vosk.Model(args.model)

    
    dump_fn = None

    with sd.RawInputStream(samplerate=args.samplerate, blocksize = 8000, device=args.device, dtype='int16',
                            channels=1, callback=callback):
            engine = pyttsx3.init()
            zh_voice_id = "zh"
 
            # 用语音包ID来配置engine
            engine.setProperty('voice', zh_voice_id)
            print(engine.getProperty('voice'))
            engine.say('请读出：'+args.word)
            engine.runAndWait()
            
            print('#' * 80)
            print('Press Ctrl+C to stop, word is {}'.format(args.word))
            print('#' * 80)

            #time.sleep(500)
            rec = vosk.KaldiRecognizer(model, args.samplerate)
            while True:
                data = q.get()
                if rec.AcceptWaveform(data):
                    ans=json.loads(rec.Result())['text']
                    got=Levenshtein.distance(str(ans).replace(' ',''),args.word)
                    if got<=len(args.word)-1:
                        print('成功识别')
                        parser.exit(0)
                    else:
                        print('识别失败')
                    print('{}词汇{}'.format(got,ans))
                else:
                    print(rec.PartialResult())
                    pass
                if dump_fn is not None:
                    dump_fn.write(data)

except KeyboardInterrupt:
    print('\nDone')
    parser.exit(0)
except Exception as e:
    parser.exit(type(e).__name__ + ': ' + str(e))
