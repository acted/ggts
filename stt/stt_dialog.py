from PySide6.QtWidgets import  QMessageBox, QDialog
from PySide6.QtCore import QTimer ,Slot
from qt_for_python.uic.read import Ui_Dialog
from PySide6.QtGui import QCloseEvent
from stt.stt_QThread import MeowSTT

class STTDialog(QDialog):

    __canClose:bool
    __totalTime:int
    __autoClose:bool
    __timer:QTimer
    __stt:MeowSTT

    def __init__(self, word:str,ask:str,time:int,acc:int,parent=None,canClose=True,autoClose=True) -> None:
        super().__init__(parent=parent)
        self.ui=Ui_Dialog()
        self.ui.setupUi(self)
        self.__canClose=canClose
        self.__autoClose=autoClose
        self.__stt=MeowSTT(word=word,parent=self,acc=int(acc))
        self.__time=time
        self.__totalTime=time
        self.__timer=QTimer(self)
        self.__timer.timeout.connect(self.secondPassed)
        self.__stt.singalSuccess.connect(self.successed)
        self.__stt.singalErr.connect(self.addWordErr)
        self.__stt.singalOther.connect(self.addWordOther)
        self.__stt.start()
        self.ui.plainTextEdit_Ask.setPlainText(ask)
        self.__timer.start(1000)

    @Slot(str)
    def addWordOther(self,word:str):
        self.ui.plainTextEdit_Log.setPlainText(self.ui.plainTextEdit_Log.toPlainText()+'\n信息：'+word)

    @Slot(str)
    def addWordErr(self,word:str):
        self.ui.plainTextEdit_Log.setPlainText(self.ui.plainTextEdit_Log.toPlainText()+'\n错误：'+word)
    
    @Slot()
    def successed(self):
        if self.__autoClose:
            self.accept()
        else:
            self.ui.pushButton.setEnabled(True)
        pass

    @Slot()
    def secondPassed(self):
        self.__time -=1
        if self.__time<0:
            print('时间已过')
            self.__canClose=True
            self.__timer.stop()
            self.ui.progressBar.setValue(0)
            self.reject()
        else:
            self.ui.progressBar.setValue(100-int(self.__time/self.__totalTime*100))
        pass

    @Slot()
    def reject(self) -> None:
        if self.__canClose:
            self.__stt.quit()
            self.__stt.wait()
            self.__timer.deleteLater()
            return super().reject()
        else:
            QMessageBox.critical(self,'错误','您不能关闭此对话框')
    
    def closeEvent(self, arg__1: QCloseEvent) -> None:
        if self.__canClose:
            self.reject()
            return super().closeEvent(arg__1)
        else:
            arg__1.ignore()
