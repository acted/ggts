#用于STT语音识别的模块，基于VOSK
from PySide6.QtCore import QThread, Signal, QCoreApplication
import sys
import os
import vosk
import sounddevice
import json
import Levenshtein
import queue


class MeowSTT(QThread):
    singalSuccess = Signal()
    singalErr = Signal(str)
    singalOther=Signal(str)
    __word:str
    __acc=1
    __model:vosk.Model
    __samplerate:int
    __queue:queue.Queue # 语音队列
    __shouldRunning=True# 是否循环

    def __init__(self, word: str, parent=None, acc=1,samplerate=None, model='vosk-model-small-cn-0.3'):
        QThread.__init__(self, parent)
        model = os.path.dirname(os.path.abspath(__file__))+'/model/'+model
        if not os.path.exists(model):# 检查语音识别模型
            print(
                "Please download a model for your language from https://alphacephei.com/vosk/models")
            print("and unpack as 'model' in the current folder.")
            self.singalErr.emit('NO MODEL FOUNDED')
        self.__model = vosk.Model(model)
        if samplerate is None:# 识别采样率
            device_info = sounddevice.query_devices(None, 'input')
            # soundfile expects an int, sounddevice provides a float:
            self.__samplerate = int(device_info['default_samplerate'])
        else:
            self.__samplerate = samplerate
        if word is None:# 如果没定义需要读出的单词
            self.singalErr.emit('NO WORD')
        self.__word = word
        self.__acc=acc # 识别准确率
        print(word)
        self.__queue = queue.Queue()

    def run(self): 
        def callback(indata, frames, time, status):
            """This is called (from a separate thread) for each audio block."""
            if status:
                print(status, file=sys.stderr)
            self.__queue.put(bytes(indata))
        dump_fn = None
        with sounddevice.RawInputStream(samplerate=self.__samplerate, blocksize=8000, device=None, dtype='int16',
                            channels=1, callback=callback):
            print('#' * 80)
            print('word is {}'.format(self.__word))
            print('#' * 80)

            # 启动识别
            rec = vosk.KaldiRecognizer(self.__model, self.__samplerate)
            while self.__shouldRunning:
                data = self.__queue.get()
                if rec.AcceptWaveform(data):
                    ans = json.loads(rec.Result())['text']
                    got = Levenshtein.distance(
                        str(ans).replace(' ', ''), self.__word)
                    if got <= len(self.__word)-self.__acc:
                        print('成功识别')
                        self.singalSuccess.emit()
                        break
                    else:
                        print('识别失败')
                        self.singalOther.emit('系统识别到：'+ans)
                else:
                    print(rec.PartialResult())
                    pass
                if dump_fn is not None:
                    dump_fn.write(data)
    
    def quit(self) -> None:
        self.__shouldRunning=False
        return super().quit()
           
    def exit(self, retcode: int = ...) -> None:
        self.__shouldRunning=False
        return super().exit(retcode)

if __name__=="__main__":
    app=QCoreApplication()
    aa=MeowSTT('测试')
    aa.start()
    app.exec()