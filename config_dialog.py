from PySide6.QtWidgets import  QMessageBox, QFileDialog,QDialog
from PySide6.QtCore import Signal ,Slot
from qt_for_python.uic.configFile import Ui_Dialog
from PySide6.QtGui import QCloseEvent
import sys,os

class ConfigDialog(QDialog):# 这是配置文件选取器，会收集一个正确的文件名作为返回。如没有收集到，系统退出

    fileAccepted = Signal(str)  

    def __init__(self, parent=None) -> None:
        super().__init__(parent=parent)
        self.ui=Ui_Dialog()
        self.ui.setupUi(self)
        
        
    @Slot()
    def accept(self) -> bool:
        if os.path.splitext(self.ui.lineEdit.text())[-1][1:]=="xlsx": # 只允许读取xlsx文件
            self.fileAccepted.emit(self.ui.lineEdit.text())
            return super().accept()
        else:
            QMessageBox.critical(self,'失败','您选中的不是MicroSoft Excel文件或不可读取')
            return False

    @Slot()
    def fileChoose(self):
        self.ui.lineEdit.setText(QFileDialog.getOpenFileName(self,"选取文件",'.','Excel files (*.xlsx)')[0])

    @Slot()
    def closeEvent(self, arg__1: QCloseEvent) -> None:
        self.reject()
        return super().closeEvent(arg__1)
    