# Good Girl Training System

受到 [bondageprojects.com](https://www.bondageprojects.com/) asylum 中 GGTS 的启发，正努力尝试将它实际制作出来！

项目目前使用中文，后续可能考虑利用 PySide6 进行国际化。

目前计划以 AGPL V3 开源此项目，项目所利用到的其他开源项目请参见项目模块内说明。

因为用了match-case所以要Python3.10以上！

## 项目说明

项目计划使用 `vosk` `pyttsx3` `pyside6`  `openpose`实现

当前完成了：文本转语音、语音转文本的模块化实现，初步的配置文件读取

下一步计划：先完成配置文件的读取和设定，让一个基本可运行的原型可以使用

项目主体结构（目前）

- openpose：计划未来制作人体动作姿态识别
- qt_for_python：PySide6-uic生成的文件
- tts：基于pyttsx3的语音转文本模块
- uis：PySide6-designer生成的文件
- stt：语音转文本模块
- main.py：程序入口文件
- config_dialog.py：配置文件选取器

项目中带有test标记的文件是模块测试文件，并未完工，与主项目关系不大，未来将删除。

### 项目计划

创建一个以 QThread 模块化的主控系统和语音命令与识别、姿态识别的系统

最后有时间的话（可能在姿态识别完成之前）做一些HTTP模块来操作别的设备（如郊狼、炮机）实现全面绒布球玩具联动

## 求帮助

因为开发者是个不太懂程序的业余绒布球，实际上并不太会搞 AI ~~只会做爱~~

姿态识别的目标是只通过一个 WebCam 识别绒布球姿态，这，看起来很难。

为了快速开发使用了 Qt widget, 如果有球有闲功夫可以搞成QtQuick向手机移植就，很有意思！！！
