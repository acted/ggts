# 用于TTS语音的QThread模块
from PySide6.QtCore import QThread,Signal,Slot
import pyttsx3
class MeowTTS(QThread):
    singalOK=Signal()
    __listForRead=[]
    __shouldRunning=True
    
    def __init__(self,language='zh',parent=None):
        super().__init__(parent)
        self.__language=language
        self.__engine = pyttsx3.init()
        self.__engine.setProperty('voice', language)

        pass
    
    @Slot()
    def readStr(self,str2say:str):
        self.__listForRead.append(str2say)
        
    
    def __read(self,str2say:str):
        self.__engine.say(str2say)
        self.__engine.runAndWait()
        self.singalOK.emit()#语音已成功播放
        
    def quit(self) -> None:
        return super().quit()
    
    def run(self):#启动后立即进入事件循环，等候readStr的槽函数
        while(self.__shouldRunning):
            if len(self.__listForRead)>0:
                for i in self.__listForRead:
                    self.__read(i)
                    self.__listForRead.remove(i)
                    
    def exit(self, retcode: int = ...) -> None:
        self.__shouldRunning=False
        return super().exit(retcode)
    
    def quit(self) -> None:
        self.__shouldRunning=False
        return super().quit()
    
    def start(self):
        super().start()
        pass