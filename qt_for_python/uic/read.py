# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'read.ui'
##
## Created by: Qt User Interface Compiler version 6.3.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QGridLayout, QLabel,
    QPlainTextEdit, QProgressBar, QPushButton, QSizePolicy,
    QWidget)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(400, 300)
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        font = QFont()
        font.setFamilies([u"Noto Sans CJK SC"])
        font.setPointSize(20)
        self.label.setFont(font)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 2)

        self.progressBar = QProgressBar(Dialog)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setValue(0)
        self.progressBar.setTextVisible(False)

        self.gridLayout.addWidget(self.progressBar, 1, 0, 1, 2)

        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAutoFillBackground(False)

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.plainTextEdit_Ask = QPlainTextEdit(Dialog)
        self.plainTextEdit_Ask.setObjectName(u"plainTextEdit_Ask")

        self.gridLayout.addWidget(self.plainTextEdit_Ask, 2, 1, 1, 2)

        self.plainTextEdit_Log = QPlainTextEdit(Dialog)
        self.plainTextEdit_Log.setObjectName(u"plainTextEdit_Log")

        self.gridLayout.addWidget(self.plainTextEdit_Log, 3, 0, 1, 3)

        self.pushButton = QPushButton(Dialog)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setEnabled(False)

        self.gridLayout.addWidget(self.pushButton, 0, 2, 2, 1)


        self.retranslateUi(Dialog)
        self.pushButton.clicked.connect(Dialog.accept)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"GGTS-VoiceDialog", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"GGTS-\u8bed\u97f3\u8bc6\u522b", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"ASK:", None))
        self.plainTextEdit_Ask.setPlainText("")
        self.plainTextEdit_Log.setPlainText("")
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"\u7ee7\u7eed", None))
    # retranslateUi

