from qt_for_python.uic.main import Ui_MainWindow
from tts.tts_QThread import MeowTTS

import sys, openpyxl
from PySide6.QtWidgets import QApplication, QMainWindow
from PySide6.QtCore import Signal, Slot
from stt.stt_dialog import STTDialog
from config_dialog import ConfigDialog
from PySide6.QtGui import QCloseEvent
class MainWindow(QMainWindow):
    signalRead=Signal(str)#发送此信号以朗读
    __configFilePath=''
    __config={
        'rbqName':'NotSet!',
        'rbqLevel':-1,
        'STTacc':1,
        'configInfo':'没有信息'
    }
    __configFile:openpyxl

    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.tts=MeowTTS('zh',self)
        self.signalRead.connect(self.tts.readStr)
        self.tts.start()
        pass
    
    def show(self):
        self.signalRead.emit('Good Girl Training System 系统已经启动')
        super().show()
        pass
    
    def closeEvent(self, event: QCloseEvent) -> None:
        self.tts.quit()
        self.tts.wait()
        return super().closeEvent(event)
    
    @Slot(str)
    def setConfigFile(self,configFilePath):
        self.__configFilePath=configFilePath
        self.__configFile=openpyxl.load_workbook(self.__configFilePath)
        configMain=self.__configFile['main']# 将主配置表中的所有配置读入
        for row in configMain.rows:
            self.__config[str(row[0].value)]=row[1].value
        self.__setSysInfo1(self.__config['configInfo'])# 初始化绒布球系统信息
        self.__setSysInfo2()
        pass
    
    def setSysInfo2(self):# TODO:在此次完成配置文件读取工作
        pass
    
    def __setSysInfo1(self,info): # 设置左侧的系统信息
        html = '''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
                <html><head><meta name="qrichtext" content="1" /><style type="text/css">
                p, li {{ white-space: pre-wrap; }}
                </style></head><body style=" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;">
                <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-weight:600;">训练中绒布球：</span><span style=" text-decoration: underline;">{rbqName}</span></p>
                <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-weight:600;">绒布球等级：</span><span style=" text-decoration: underline;">{rbqLevel}</span></p>
                <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-weight:600;">系统提示：</span></p>
                <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">{sysInfo}</p>
                <p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p>
                <p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><br /></p>
                <hr />
                <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-weight:600;">系统模式：</span><span style=" text-decoration: underline;">手动模式</span></p>
                <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-weight:600;">系统版本：</span><span style=" text-decoration: underline;">0.2</span></p>
                <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-weight:600;">系统加载模块：</span><span style=" text-decoration: underline;">VoiceZH;ByPass</span></p>
                <p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-weight:600;">系统配置文件：</span><span style=" text-decoration: underline;">{configFile}</span></p></body></html>'''
        self.ui.textEdit.setHtml(html.format(rbqName=self.__config['rbqName'],rbqLevel=self.__config['rbqLevel'],configFile=self.__configFilePath,sysInfo=info))
        pass


    def button1(self):
        self.gg=STTDialog('请狠狠的日我','回答：请狠狠的日我',100,self.__config['STTacc'],self)
        self.gg.open()
    
    def button2(self):
        self.signalRead.emit('通用按钮2已被按下')
    
    def button3(self):
        self.signalRead.emit('通用按钮3已被按下')
    
    def button4(self):
        self.signalRead.emit('通用按钮4已被按下')
        
    


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    configDialog=ConfigDialog(window)
    configDialog.fileAccepted.connect(window.setConfigFile)
    #configDialog.accepted.connect(window.show)
    if configDialog.exec()==1:
        window.show()
        sys.exit(app.exec())
    else:
        sys.exit(0)
